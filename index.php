<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require __DIR__ . '/autoload.php';
require __DIR__ . '/lib/Mobile_Detect.php';

use MaxMind\Db\Reader;


$active = true;


$data = [
    'ios' => [
        'geo' => [
            'NZ' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342473&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ],
            'SG' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342473&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ],
            'US' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342473&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ],
            'TR' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342473&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ]
        ],
        'urls' => [
            'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=122288118&site_id=42743&clk_uid={$click_id}&subsite_id={$source_id}',
            'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=1489485&site_id=42743&clk_uid={$click_id}&subsite_id={$source_id}'
        ]
    ],
    'android' => [
        'geo' => [
            'KR' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342459&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ],
            'DE' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342459&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ],
            'FR' => [
                'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=192342459&site_id=43233&clk_uid={$click_id}&subsite_id={$source_id}',
            ]
        ],
        'urls' => [
            'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=121942997&site_id=42743&clk_uid={$click_id}&subsite_id={$source_id}',
            'https://cpactions.com/api/v1.0/clk/track/proxy?ad_id=164855069&site_id=42743&clk_uid={$click_id}&subsite_id={$source_id}'
        ]
    ],
    'url' => 'https://cpactions.com/api/v1.0/clk/track/proxy?disable_uu_validation=1&ad_id=1489485&site_id=9840&subsite_id={$source_id}&click_id={$click_id}'
    //'url' => 'https://yahoo.com'
];


/**
 * http://159.65.127.57/?ip=24.24.24.24&source_id=1&click_id=2
 **/

header('Location: ' . check($data, $active), true, 303);
exit();

function check($data, $active)
{
    $memcached = new Memcached();
    $memcached->addServer('127.0.0.1', 11211);


    $ip = get_ip_address();
    $reader = new Reader(__DIR__ . '/db/GeoIP2.mmdb');
    $connection = $reader->get($ip);
    $reader->close();


    $vars = array(
        '{$click_id}' => $_GET['click_id'],
        '{$source_id}' => $_GET['source_id'],
    );

    try {
        if (!$active) {
            throw new RuntimeException('empty');
        }


        $message = $ip . ', ' . $connection['connection_type'];
        if (isset($_GET['ip']) && $_GET['ip'] != $ip) {
            $message .= ', ' . $_GET['ip'];
        }

        if (count($connection)) {
            //echo 'Тип соединения '.$connection['connection_type'].'<br />';
            if ($connection['connection_type'] == 'Cellular') {

                $detect = new Mobile_Detect;

                if ($detect->isiOS()) {
                    $links = $data['ios'];
                    $mem_name = 'ios';
                } elseif ($detect->isAndroidOS()) {
                    $links = $data['android'];
                    $mem_name = 'android';
                } else {
                    throw new RuntimeException('empty');
                }

                //echo 'ОС '.$mem_name.'<br />';

                $urls = $links['urls'];

                $reader = new Reader(__DIR__ . '/db/GeoLite2-Country.mmdb');
                $country = $reader->get($ip);
                $reader->close();

                if(isset($links['geo'][$country['country']['iso_code']])){
                    $urls = $links['geo'][$country['country']['iso_code']];
                    $mem_name .= '_geo';
                } else {
                    $mem_name .= '_link';
                }

                $result = $memcached->get($mem_name);

                if ($result === false || $result >= count($urls) - 1) {
                    $number = 0;
                } else {
                    $number = $result + 1;
                }
                $message .= ' LINK: ' . $number;
                $message .= ', ' . $_GET['click_id'];
                $message .= ', ' . $_GET['source_id'];
                writeLog($message, 1);
                $memcached->set($mem_name, $number);
                return strtr($urls[$number], $vars);
            }
        } else {
            throw new RuntimeException('empty');
        }
        throw new RuntimeException('empty');
    } catch (Exception $exception) {
        $message = $ip . ', ' . $connection['connection_type'] . ', ' . $exception->getMessage();
        if (isset($_GET['ip']) && $_GET['ip'] != $ip) {
            $message .= ', ' . $_GET['ip'];
        }
        $message .= ', ' . $_GET['click_id'];
        $message .= ', ' . $_GET['source_id'];
        writeLog($message, 2);
        return strtr($data['url'], $vars);
    }
}

function writeLog($log, $url)
{
    file_put_contents(__DIR__ . '/logs/log_' . $url . '_' . date('j.n.Y') . '.txt', $log . PHP_EOL, FILE_APPEND);
}

/**
 * Retrieves the best guess of the client's actual IP address.
 * Takes into account numerous HTTP proxy headers due to variations
 * in how different ISPs handle IP addresses in headers between hops.
 */
function get_ip_address()
{
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
        return $_SERVER['HTTP_FORWARDED'];
    }
    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 * @param $ip
 * @return bool
 */
function validate_ip($ip)
{
    if (strtolower($ip) === 'unknown') {
        return false;
    }
    // generate ipv4 network address
    $ip = ip2long($ip);
    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) {
            return false;
        }
        if ($ip >= 167772160 && $ip <= 184549375) {
            return false;
        }
        if ($ip >= 2130706432 && $ip <= 2147483647) {
            return false;
        }
        if ($ip >= 2851995648 && $ip <= 2852061183) {
            return false;
        }
        if ($ip >= 2886729728 && $ip <= 2887778303) {
            return false;
        }
        if ($ip >= 3221225984 && $ip <= 3221226239) {
            return false;
        }
        if ($ip >= 3232235520 && $ip <= 3232301055) {
            return false;
        }
        if ($ip >= 4294967040) {
            return false;
        }
    }
    return true;
}